﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// Add
using System.IO;
using NLog.Web;

using WebBreakingNews.Parts;

namespace WebBreakingNews
{
    public class Program
    {
        
        public static void Main(string[] args)
        {
            // 同時接続数 はじめ "2"なので、 "5000"にしてみた
            System.Net.ServicePointManager.DefaultConnectionLimit = 5000;
            //int limitcnt = System.Net.ServicePointManager.DefaultConnectionLimit; // Readしてみた
            // 以下は古い？
            //int c = System.Net.Http.HttpClientHandler.MaxConnectionsPerServe;
            
            // NLog 開始
            var logger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
            try
            {
                logger.Debug("init main");
                var host = CreateHostBuilder(args).Build();

                // ここで、DI(Delendency Injection) したい場合、記載するとのこと
                // Startup.cs の  ConfigureServices() に
                // services.AddTransient <interface名.継承クラス名>();　で記載する方法もある。
                //
                //using (var scope = host.Services.CreateScope())
                //{
                //    var services = scope.ServiceProvider;
                //    try
                //    {
                //        var myDependency = services.GetRequiredService<IMyDependency>();
                //        myDependency.WriteMessage("Call services from main"); // あくまでも例。
                //    }
                //    catch (Exception ex)
                //    {
                //        var logger = services.GetRequiredService<ILogger<Program>>();
                //        logger.LogError(ex, "An error occurred.");
                //    }
                //}

                // wordInfo Object 生成
                WordInfo wordInfo = WordInfo.GetInstance(WordInfo.JPN);
               
                // Start
                host.Run();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Main() Stopped program because of exception");
                throw;
            }
            finally
            {
                NLog.LogManager.Shutdown();
            }
        }

        // NLog用に以下追加　start
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => {
                    webBuilder.UseStartup<Startup>();
                })
                .ConfigureLogging(logging => {
                    logging.ClearProviders();
                    logging.SetMinimumLevel(LogLevel.Trace);
                })
                .UseNLog();
        // end
       
        
    }
}
