﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebBreakingNews.Parts
{
    ///---------------------------------------------------------------
    /// <summary>
    /// SQL Serverへの接続文を返す　Singletoneクラス
    /// </summary>
    ///---------------------------------------------------------------
    public class SqlConnectionString
    {
        private static SqlConnectionString instance;
        public static SqlConnectionString Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SqlConnectionString();
                }
                return instance;
            }
        }

        // SQL Serverへ接続し、DB名を取得するとき用
        //public readonly string master =  String.Format(@"Data Source={0};Initial Catalog=master;User Id=takani;Password=takani;", Environment.MachineName + "\\SQLEXPRESS");
        public readonly string master = String.Format(@"Data Source={0};Initial Catalog=master;User Id=sa;Password=tecrun20150807!;", "133.242.229.64,2000");


        // SQL Serverへ DB名指定で接続し、テーブルアクセスするとき用
        public string GetSqlConnection(string dbName)
        {
            //string sqlConnectionString = String.Format(@"Data Source={0};Initial Catalog={1};User ID=takani;Password=takani;", Environment.MachineName + "\\SQLEXPRESS", value);
            string sqlConnectionString = String.Format(@"Data Source={0};Initial Catalog={1};User ID=sa;Password=tecrun20150807!;", "133.242.229.64,2000", dbName);
            return sqlConnectionString;

        }

        //public static  Microsoft.Data.SqlClient.SqlConnectionStringBuilder(string dbName) 
        //{
        //    var builder = new Microsoft.Data.SqlClient.SqlConnectionStringBuilder();
        //    // 接続する先の SQL Server インスタンスの名前またはネットワーク アドレスを取得または設定
        //    //builder.DataSource = @".\\TWJPC004\\SQLEXPRESS";
        //    //builder.DataSource = Environment.MachineName + "\\SQLEXPRESS";  
        //    builder.DataSource = "133.242.229.64,2000";
        //
        //    // 接続に関連付けられたデータベースの名前を取得または設定
        //    builder.InitialCatalog = dbName;
        //
        //    // User ID および Password を接続文字列中に指定    (false の場合)、
        //    // 現在の Windows アカウントの資格情報を認証に使用 (true の場合) 
        //    //builder.IntegratedSecurity = true;                                                 
        //    builder.IntegratedSecurity = false;
        //
        //    //builder.UserID = "takani";
        //    //builder.Password = "takani";
        //    builder.UserID = "sa";
        //    builder.Password = "tecrun20150807!";
        //
        //    return builder;
        //}

}
}
