﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// Add
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iFont = iTextSharp.text.Font;
using System.Xml.Linq;
using WebBreakingNews.Models;



namespace WebBreakingNews.Parts
{
    ///---------------------------------------------------------------
    /// <summary>
    /// 本アプリケーションで使う部品 Method群 クラス
    /// </summary>
    ///---------------------------------------------------------------
    public class MyParts
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public static readonly int NO_ERR = 0;
        public static readonly int ERR_OTHER = -1;
        public static readonly int ERR_IMAGE_FORMAT = -2;

        ///---------------------------------------------------------------
        /// <summary>
        /// playerInfo.PRINT_POSITION1 を解析し、XMLで記載された各パラメータ値を
        /// playerInfo.xxx に保存して返す。
        /// </summary>
        /// <param name="playerInfo">選手結果情報</param>
        /// <returns>なし</returns>
        ///---------------------------------------------------------------
        public bool AnalyzeXmlData(ref PlayerInfo playerInfo)
        {
            bool bret = true;
            try
            {
               
                string rdXmlData = playerInfo.PRINT_POSITION1;
                Stream xmlStreamData = new MemoryStream(System.Text.Encoding.Unicode.GetBytes(rdXmlData));

                //System.IO.StreamReader sr = new System.IO.StreamReader(
                //    @"C:\test\sample.xml", new System.Text.UTF8Encoding(false));
                System.Xml.Serialization.XmlSerializer serializer =
                   new System.Xml.Serialization.XmlSerializer(typeof(PrintSettings));
                PrintSettings xmlDataList = (PrintSettings)serializer.Deserialize(xmlStreamData);
                xmlStreamData.Close();


                // 表示項目名取得
                bool tmpBool;
                playerInfo.dispResultTitle.dispCount = 0;

                if (xmlDataList.English != null)
                {
                    tmpBool = playerInfo.dispResultTitle.dispItem;
                    bool.TryParse(xmlDataList.English, out tmpBool);
                    playerInfo.dispResultTitle.dispItem = tmpBool;
                }

                // Japanese then
                if (playerInfo.dispResultTitle.dispItem == false)
                {
                    if (xmlDataList.RaceJP != null)
                    {
                        playerInfo.dispResultTitle.Race = xmlDataList.RaceJP;
                        playerInfo.dispResultTitle.dispItem = true;
                        playerInfo.dispResultTitle.dispCount++;
                    }
                    if (xmlDataList.BibNumberJP != null)
                    {
                        playerInfo.dispResultTitle.BibNumber = xmlDataList.BibNumberJP;
                        playerInfo.dispResultTitle.dispNumber = true;
                        playerInfo.dispResultTitle.dispCount++;
                    }
                    if (xmlDataList.NameJP != null)
                    {
                        playerInfo.dispResultTitle.Name = xmlDataList.NameJP;
                        playerInfo.dispResultTitle.dispName = true;
                        playerInfo.dispResultTitle.dispCount++;
                    }
                    if (xmlDataList.GrossTimeJP != null)
                    {
                        playerInfo.dispResultTitle.GrossTime = xmlDataList.GrossTimeJP;
                        playerInfo.dispResultTitle.dispGrossTime = true;
                        playerInfo.dispResultTitle.dispCount++;
                    }
                    if (xmlDataList.NetTimeJP != null)
                    {
                        playerInfo.dispResultTitle.NetTime = xmlDataList.NetTimeJP;
                        playerInfo.dispResultTitle.dispNetTime = true;
                        playerInfo.dispResultTitle.dispCount++;
                    }
                    if (xmlDataList.PlaceJP != null)
                    {
                        playerInfo.dispResultTitle.Place = xmlDataList.PlaceJP;
                        playerInfo.dispResultTitle.dispPlace = true;
                        playerInfo.dispResultTitle.dispCount++;
                    }
                    if (xmlDataList.PlaceByAllJP != null)
                    {
                        playerInfo.dispResultTitle.PlaceByAll = xmlDataList.PlaceByAllJP;
                        playerInfo.dispResultTitle.dispPlaceByAll = true;
                        playerInfo.dispResultTitle.dispCount++;
                    }
                }
                // English then
                else
                {
                    if (xmlDataList.RaceENG != null)
                    {
                        playerInfo.dispResultTitle.Race = xmlDataList.RaceENG;
                        playerInfo.dispResultTitle.dispItem = true;
                        playerInfo.dispResultTitle.dispCount++;
                    }
                    if (xmlDataList.BibNumberENG != null)
                    {
                        playerInfo.dispResultTitle.BibNumber = xmlDataList.BibNumberENG;
                        playerInfo.dispResultTitle.dispNumber = true;
                        playerInfo.dispResultTitle.dispCount++;
                    }
                    if (xmlDataList.NameENG != null)
                    {
                        playerInfo.dispResultTitle.Name = xmlDataList.NameENG;
                        playerInfo.dispResultTitle.dispName = true;
                        playerInfo.dispResultTitle.dispCount++;
                    }
                    if (xmlDataList.GrossTimeENG != null)
                    {
                        playerInfo.dispResultTitle.GrossTime = xmlDataList.GrossTimeENG;
                        playerInfo.dispResultTitle.dispGrossTime = true;
                        playerInfo.dispResultTitle.dispCount++;
                    }
                    if (xmlDataList.NetTimeENG != null)
                    {
                        playerInfo.dispResultTitle.NetTime = xmlDataList.NetTimeENG;
                        playerInfo.dispResultTitle.dispNetTime = true;
                        playerInfo.dispResultTitle.dispCount++;
                    }
                    if (xmlDataList.PlaceENG != null)
                    {
                        playerInfo.dispResultTitle.Place = xmlDataList.PlaceENG;
                        playerInfo.dispResultTitle.dispPlace = true;
                        playerInfo.dispResultTitle.dispCount++;
                    }
                    if (xmlDataList.PlaceByAllENG != null)
                    {
                        playerInfo.dispResultTitle.PlaceByAll = xmlDataList.PlaceByAllENG;
                        playerInfo.dispResultTitle.dispPlaceByAll = true;
                        playerInfo.dispResultTitle.dispCount++;
                    }
                }

                // 座標、サイズ関連情報取得
                double tmpDbl;
                int tmpInt;

                if (xmlDataList.ImageFrameHeight != null)
                {
                    tmpDbl = playerInfo.ImgFrameHeight;
                    double.TryParse(xmlDataList.ImageFrameHeight, out tmpDbl);
                    playerInfo.ImgFrameHeight = tmpDbl;
                }
                if (xmlDataList.ImageFrameWidth != null)
                {
                    tmpDbl = playerInfo.ImgFrameWidth;
                    double.TryParse(xmlDataList.ImageFrameWidth, out tmpDbl);
                    playerInfo.ImgFrameWidth = tmpDbl;
                }

                if (xmlDataList.ImageX != null)
                {
                    tmpDbl = playerInfo.ImgXmm;
                    double.TryParse(xmlDataList.ImageX, out tmpDbl);
                    playerInfo.ImgXmm = tmpDbl;
                }
                if (xmlDataList.ImageY != null)
                {
                    tmpDbl = playerInfo.ImgYmm;
                    double.TryParse(xmlDataList.ImageY, out tmpDbl);
                    playerInfo.ImgYmm = tmpDbl;
                }
                if (xmlDataList.ImageMagnifi != null)
                {
                    tmpDbl = playerInfo.ImageMagnifi;
                    double.TryParse(xmlDataList.ImageMagnifi, out tmpDbl);
                    playerInfo.ImageMagnifi = tmpDbl;
                }

                if (xmlDataList.FontType != null)
                {
                    playerInfo.dispResultTitle.fontKind = xmlDataList.FontType;
                }
                if (xmlDataList.FontSize != null)
                {
                    float tmpFlt = playerInfo.dispResultTitle.fontSize;
                    float.TryParse(xmlDataList.FontSize, out tmpFlt);
                    playerInfo.dispResultTitle.fontSize = tmpFlt;
                }
                if (xmlDataList.BetweenRow != null)
                {
                    tmpDbl = playerInfo.rowMargin;
                    double.TryParse(xmlDataList.BetweenRow, out tmpDbl);
                    playerInfo.rowMargin = tmpDbl;
                }
                if (xmlDataList.ItemTitleX != null)
                {
                    tmpDbl = playerInfo.ItemTitleX;
                    double.TryParse(xmlDataList.ItemTitleX, out tmpDbl);
                    playerInfo.ItemTitleX = tmpDbl;
                }
                if (xmlDataList.ItemTitleY != null)
                {
                    tmpDbl = playerInfo.ItemTitleY;
                    double.TryParse(xmlDataList.ItemTitleY, out tmpDbl);
                    playerInfo.ItemTitleY = tmpDbl;
                }
                if (xmlDataList.ItemContentsX != null)
                {
                    tmpDbl = playerInfo.ItemContentsX;
                    double.TryParse(xmlDataList.ItemContentsX, out tmpDbl);
                    playerInfo.ItemContentsX = tmpDbl;
                }
                if (xmlDataList.ItemContentsY != null)
                {
                    tmpDbl = playerInfo.ItemContentsY;
                    double.TryParse(xmlDataList.ItemContentsY, out tmpDbl);
                    playerInfo.ItemContentsY = tmpDbl;
                }
                if (xmlDataList.ItemCharsMax != null)
                {
                    tmpInt = playerInfo.ItemCharsMax;
                    int.TryParse(xmlDataList.ItemCharsMax, out tmpInt);
                    playerInfo.ItemCharsMax = tmpInt;
                }

            }
            catch (Exception e)
            {
                bret = false;
                logger.Error("AnalyzeXmlData() Exception = " + e.Message);
            }

            return bret;
        } // End of AnalyzeXmlData


        //---------------------------------------------------------------
        /// <summary>
        /// playerInfo をもとに、完走証ベースPDFファイルに
        /// 画像と文字列を挿入した完走証PDFファイルのbyte[]データ を返す。
        /// </summary>
        /// <param name="playerInfo">走証 や 結果、選手情報</param>
        /// /// <param name="imgStream">挿入画像ファイル</param>
        /// <returns>画像と文字列を挿入した完走証PDFファイル</returns>
        ///---------------------------------------------------------------
        public byte[] InsertImageAndStringIntoPDF(PlayerInfo playerInfo, MemoryStream imgStream, ref int errKind)
        {
            errKind = NO_ERR;
            PdfReader reader = null;
            PdfStamper stamper = null;
            byte[] pdfFileBinaryData;
            try
            {
                reader = new PdfReader(playerInfo.PDF);
                //// テンプレの1ページ目のページサイズを取得
                //var size = reader.GetPageSize(1);
                //// 開いたファイルのサイズでドキュメントを作成
                ////document = new Document(size);
                ////document.Open();
            
                // 元Pdf を Stamper で開始。出力を MemoryStreamへ
                MemoryStream outFileStream = new MemoryStream();
                stamper = new PdfStamper(reader, outFileStream);
                var pdfContentByte = stamper.GetOverContent(1);

                System.Collections.Hashtable info = new System.Collections.Hashtable();
                // ハッシュに値を設定する
                info["Title"] = playerInfo.EventName;
                //info["Title"] = "result.pdf";

                // Stamperにハッシュテーブルの内容を入れて
                stamper.MoreInfo = info;

                // 印刷、Copy、アクセシビリティのための内容抽出 を許可
                int permission = PdfWriter.AllowPrinting | PdfWriter.AllowCopy | PdfWriter.AllowScreenReaders;
                stamper.SetEncryption(null, null, permission, true);
                stamper.RotateContents = false; // ページ回転禁止

                
                //===== 文字列挿入 =====

                //  以下、No data is available for encoding 1252. 対策。
                //    ASP.NET Core 対応してないらしく、以下追加
                System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

                // デフォルト メイリオ
                //if(playerInfo.dispResultTitle.fontKind.Equals("メイリオ"))
                BaseFont fnt = BaseFont.CreateFont(@"c:\windows\fonts\meiryo.ttc,0", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
                if (playerInfo.dispResultTitle.fontKind.Equals("MSゴシック"))
                    fnt = BaseFont.CreateFont(@"c:\windows\fonts\msgothic.ttc,0", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);
                if (playerInfo.dispResultTitle.fontKind.Equals("MS明朝"))
                    fnt = BaseFont.CreateFont(@"c:\windows\fonts\msmincho.ttc,0", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);

                // 文字列挿入開始 位置計算
                float titleX = (float)mmToPoint(playerInfo.ItemTitleX);     // 項目タイトル用
                float titleY = (float)mmToPoint(playerInfo.ItemTitleY);
                //float contX = (float)mmToPoint(playerInfo.ItemContentsX);   // 項目内容用
                float contY = (float)mmToPoint(playerInfo.ItemContentsY);
                //float rowMargin = (float)mmToPoint(playerInfo.rowMargin);
                float fntSizeFlt = playerInfo.dispResultTitle.fontSize;     // fontSize
                //int itemCharsMaxt = playerInfo.ItemCharsMax;                // 項目内容1行表示最大文字数。これ超えたら改行

                // 取得時に数えた 表示項目数 を計算し、一番下の項目の Y座標を計算
                // ( 表示しない項目数 x FontSize 分指定位置で最後の項目から描画するように計算 )
                int notDispCount = playerInfo.dispResultTitle.dispMaxCount - playerInfo.dispResultTitle.dispCount;
                titleY = titleY + (float)notDispCount * fntSizeFlt;
                contY = contY + (float)notDispCount * fntSizeFlt;


                //===== 文字列描画開始 =====

                pdfContentByte.BeginText();
                pdfContentByte.SetFontAndSize(fnt, fntSizeFlt);    // fontSize設定

                //// 項目文字の最大文字数取得
                //int maxCharCount = GetTileCharMaxCount(playerInfo);

                // 総合順位
                if (playerInfo.dispResultTitle.dispPlaceByAll == true)
                {
                    string titleStr = playerInfo.dispResultTitle.PlaceByAll;
                    string itemStr  = playerInfo.PlaceByAll;
                    SetItemTitle(pdfContentByte, playerInfo, titleStr, itemStr, titleX, ref titleY);            // 項目名表示
                    SetItemAndCalcNextY(pdfContentByte, itemStr + "位", playerInfo, ref contY, ref titleY);     // 項目内容表示 次の行の titleY、contY も計算
                }
                // 順位
                if (playerInfo.dispResultTitle.dispPlace == true)
                {
                    string titleStr = playerInfo.dispResultTitle.Place;
                    string itemStr = playerInfo.Place;
                    SetItemTitle(pdfContentByte, playerInfo, titleStr, itemStr, titleX, ref titleY);            // 項目名表示
                    SetItemAndCalcNextY(pdfContentByte, itemStr + "位", playerInfo, ref contY, ref titleY);     // 項目内容表示 次の行の titleY、contY も計算
                }
                // ネットタイム
                if (playerInfo.dispResultTitle.dispNetTime == true)
                {

                    string titleStr = playerInfo.dispResultTitle.NetTime;
                    string itemStr = playerInfo.NetTime;
                    SetItemTitle(pdfContentByte, playerInfo, titleStr, itemStr, titleX, ref titleY);  // 項目名表示
                    SetItemAndCalcNextY(pdfContentByte, itemStr, playerInfo, ref contY, ref titleY);  // 項目内容表示 次の行の titleY、contY も計算
                }
                // グロスタイム
                if (playerInfo.dispResultTitle.dispGrossTime == true)
                {
                    string titleStr = playerInfo.dispResultTitle.GrossTime;
                    string itemStr = playerInfo.GrossTime;
                    SetItemTitle(pdfContentByte, playerInfo, titleStr, itemStr, titleX, ref titleY);  // 項目名表示
                    SetItemAndCalcNextY(pdfContentByte, itemStr, playerInfo, ref contY, ref titleY);  // 項目内容表示 次の行の titleY、contY も計算
                }
                // 名前
                if (playerInfo.dispResultTitle.dispName == true)
                {
                    string titleStr = playerInfo.dispResultTitle.Name;
                    string itemStr = playerInfo.PlayerName;
                    SetItemTitle(pdfContentByte, playerInfo, titleStr, itemStr, titleX, ref titleY);  // 項目名表示
                    SetItemAndCalcNextY(pdfContentByte, itemStr, playerInfo, ref contY, ref titleY);  // 項目内容表示 次の行の titleY、contY も計算
                }
                // ナンバー
                if (playerInfo.dispResultTitle.dispNumber == true)
                {
                    string titleStr = playerInfo.dispResultTitle.BibNumber;
                    string itemStr = playerInfo.BibNumber.ToString();
                    SetItemTitle(pdfContentByte, playerInfo, titleStr, itemStr, titleX, ref titleY);  // 項目名表示
                    SetItemAndCalcNextY(pdfContentByte, itemStr, playerInfo, ref contY, ref titleY);  // 項目内容表示 次の行の titleY、contY も計算
                }
                // 種目
                if (playerInfo.dispResultTitle.dispItem == true)
                {
                    string titleStr = playerInfo.dispResultTitle.Race;
                    string itemStr = playerInfo.ItemName;
                    SetItemTitle(pdfContentByte, playerInfo, titleStr, itemStr, titleX, ref titleY);  // 項目名表示
                    SetItemAndCalcNextY(pdfContentByte, itemStr, playerInfo, ref contY, ref titleY);  // 項目内容表示 次の行の titleY、contY も計算
                }
                pdfContentByte.EndText();


                //===== 画像追加 =====

                if (imgStream.Length != 0)
                {
                    // 挿入画像ファイル MemoryStreamデータを iTextSharpの image に代入
                    iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imgStream);

                    // 画像サイズ 取得
                    float imgHeight_px = image.Height;    // 画像縦幅(px)
                    float imgWidth_px = image.Width;      // 画像横幅(px)

                    // 確認でファイルに出力する場合。
                    //MemoryStream outImage = new MemoryStream();
                    //ResizeImageWhileMaintainingAspectRatio(imgStream, outImage, (int)140, (int)140);
                    //System.IO.File.WriteAllBytes(@"C:\\Users\\takani\\Desktop\\k.jpeg", outImage.ToArray());
                    ////System.IO.File.WriteAllBytes(@"C:\\Users\\takani\\Desktop\\k.png", imgStream.ToArray());
                    // 出力したものを MemoryStream にしたい場合
                    //outImage.Position = 0; // <--- これ必須
                    //iTextSharp.text.Image imageTmp = iTextSharp.text.Image.GetInstance(outImage);
                    //float tmpH = imageTmp.Height;
                    //float tmpW = imageTmp.Width;

                    // 挿入画像ファイル の Exfi 情報を参照するために Bitmapに変換
                    MemoryStream mStream = new MemoryStream();
                    byte[] pData = imgStream.ToArray(); 
                    mStream.Write(pData, 0, Convert.ToInt32(pData.Length));
                    using (System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(mStream))
                    {
                        //var rotation = System.Drawing.RotateFlipType.RotateNoneFlipNone;  // 複製作成時必要

                        //Exif情報を列挙
                        foreach (System.Drawing.Imaging.PropertyItem item in bmp.PropertyItems)
                        {
                            if (item.Id != 0x0112)
                                continue;

                            // IFD0 0x0112; Orientation の値を調べる
                            switch (item.Value[0])
                            {
                                case 3:
                                    // 時計回りに180度回転しているので、1時計回りに80度回転して戻す
                                    //rotation = System.Drawing.RotateFlipType.Rotate180FlipNone;   // 複製作成時必要
                                    image.Rotation = (float)Math.PI / 2;
                                    image.RotationDegrees = 180f;    // 半時計回り
                                    break;
                                case 6:
                                    // 時計回りに270度回転しているので、時計回りに90度回転して戻す
                                    //rotation = System.Drawing.RotateFlipType.Rotate90FlipNone;  // 複製作成時必要
                                    image.Rotation = (float)Math.PI / 2;
                                    image.RotationDegrees = 270f;    // 半時計回り
                                    break;
                                case 8:
                                    // 時計回りに90度回転しているので、時計回りに270度回転して戻す
                                    //rotation = System.Drawing.RotateFlipType.Rotate270FlipNone;  // 複製作成時必要
                                    image.Rotation = (float)Math.PI / 2;
                                    image.RotationDegrees = 90f;    // 半時計回り
                                    break;
                            }
                        }

                        //// 元の画像を複製する場合は、以下。
                        //using (var rotated = (System.Drawing.Bitmap)bmp.Clone())
                        //{
                        //    // 指定された角度だけ画像を回転する
                        //    rotated.RotateFlip(rotation);
                        //
                        //    // BMP形式で保存
                        //    rotated.Save("rotated.bmp", System.Drawing.Imaging.ImageFormat.Bmp);
                        //}

                    }//bmp.Dispose();


                    ////// image の縦横 px を pt に変換
                    ////    pt 文字や図形のサイズを表す単位
                    ////    1/72インチ＝１ポイント(pt)。　(最近のデバイスは、この大前提が崩れている様子。少しずれる可能性あり)
                    ////    1インチ 2.54cm(25.4mm)、1pt=0.0352777...cm
                    ////    1pt = 0.3527777...mm
                    ////    Windowsが、96ppiの解像度なので、72pt = 96px (1pt=1.33px)
                    ////    
                    //double bs = (double)(1.0/1.33);   // これを考慮すると 画像が1.3倍大きくなる
                    double bs = (double)(1.0);          // 1px = 1pt だと画像が予定通りの大きさになる
                    float imgHeight_pt = (float)((double)imgHeight_px * bs);
                    float imgWidth_pt  = (float)((double)imgWidth_px * bs);

                    // 完走証テーブルで指定された画像の枠 縦横 mm を pt に変換。
                    float frame_H_pt = (float)mmToPoint(playerInfo.ImgFrameHeight);
                    float frame_W_pt = (float)mmToPoint(playerInfo.ImgFrameWidth);

                    // 枠の縦横小さい方　を対象にする
                    float frame_pt;
                    if (frame_H_pt < frame_W_pt)
                        frame_pt = frame_H_pt;
                    else
                        frame_pt = frame_W_pt;

                    // pt で　枠に入る画像の縦、横を計算。小さい方を採用
                    //float scale = Math.Min((float)frame_W_pt / (float)imgWidth_pt, (float)frame_H_pt / (float)imgHeight_pt);
                    float scale = Math.Min((float)frame_pt / (float)imgWidth_pt, (float)frame_pt / (float)imgHeight_pt);
                    scale = scale * 100;
                   
                    image.ScalePercent(scale);                // 例：30f is 30%
                    //image.ScaleToFit(50f, 50f);             // これでも縮小できる
                    //image.ScalePercent((float)playerInfo.ImageMagnifi);
                    logger.Trace("PDF image scale = " + scale.ToString());
                    
                    
                    // 画像描画位置指定 (左下起点。右上プラス方向)
                    float image_x_pt = (float)mmToPoint(playerInfo.ImgXmm);
                    float image_y_pt = (float)mmToPoint(playerInfo.ImgYmm);
                    image.SetAbsolutePosition(image_x_pt, image_y_pt);

                    // 画像を挿入
                    pdfContentByte.AddImage(image);
                }


                ////保存後はフォーム入力不可にする
                //stamper.FormFlattening = True
                stamper.Close();

                if (reader != null)
                    reader.Close();

                // result.pdf つかわないで、MemoryStreamで出力する
                pdfFileBinaryData = outFileStream.ToArray();
                outFileStream.Dispose();

                return pdfFileBinaryData;
            }
            catch (Exception e)
            {
                if (stamper != null)
                    stamper.Close();

                if (reader != null)
                    reader.Close();

                if (e.Message.IndexOf(" not a recognized imageformat.") >= 0)//"The byte array is not a recognized imageformat.";
                    errKind = ERR_IMAGE_FORMAT;
                else
                    errKind = ERR_OTHER;

                return null;
            }
        }


        //---------------------------------------------------------------
        /// <summary>
        ///  画像(inFile)を width, height に収まる大きさにして、 outFileで返す
        /// </summary>
        /// <param name="inFile">縮小前画像</param>
        /// <param name="outFile">縮小後画像</param>
        /// <param name="width">縮小幅</param>
        /// <param name="height">縮小縦</param>
        /// <returns>mmData を pt に変換した値</returns>
        ///---------------------------------------------------------------
        public void ResizeImage(MemoryStream inFile, MemoryStream outFile,
            //System.Drawing.Imaging.ImageFormat imageFormat,
            int width, int height)
        {

            byte[] pdfFileBinaryData = inFile.ToArray();

            MemoryStream ms = new MemoryStream(pdfFileBinaryData);

            // サイズ変更する画像ファイルを開く
            using (System.Drawing.Image image = System.Drawing.Image.FromStream(ms))
            {
                // 変更倍率を取得
                float scale = Math.Min((float)width / (float)image.Width, (float)height / (float)image.Height);
                //float scale = (float)1.0; // 1倍

                // 変更サイズを取得する
                int widthToScale = (int)(image.Width * scale);
                int heightToScale = (int)(image.Height * scale);

                // サイズ変更した画像を作成
                using (System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(width, height))
                //using (System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(widthToScale, heightToScale)) // 1倍
                using (System.Drawing.Graphics graphics = System.Drawing.Graphics.FromImage(bitmap))
                {
                    // 背景色を塗る
                    System.Drawing.SolidBrush solidBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Black);
                    graphics.FillRectangle(solidBrush, new System.Drawing.RectangleF(0, 0, width, height));
                    //graphics.FillRectangle(solidBrush, new System.Drawing.RectangleF(0, 0, widthToScale, heightToScale)); // 1倍

                    //// サイズ変更した画像に、左上を起点に変更する画像を描画
                    graphics.DrawImage(image, 0, 0, widthToScale, heightToScale);
                    
                    // サイズ変更した画像を保存する
                    //bitmap.Save(destinationFile, imageFormat);
                    bitmap.Save(outFile, System.Drawing.Imaging.ImageFormat.Jpeg);

                }
            }
        }

        //---------------------------------------------------------------
        /// <summary>
        ///  mm を point(pt) に変換
        //
        ///  double mmData 単位mm の変換前データ
        ///  return dobule mmData を pt に変換した値。
        ///
        ///     1/72インチ＝１ポイント(pt)。　(最近のデバイスは、この大前提が崩れている様子。少しずれる可能性あり)
        ///     1インチ 2.54cm、1pt=0.0352777...cm
        ///     1pt = 0.3527777...mm
        /// </summary>
        /// <param name="mmData">単位mm の変換前データ</param>
        /// <returns>mmData を pt に変換した値</returns>
        ///---------------------------------------------------------------
        public double mmToPoint(double mmData)
        {
            double dblPoint = 0.0;
            double onePtTo_mm = 2.54 / 72.0 * 10;     // 1pt 何mm? => 0.3527777...mm
            if (mmData != 0.0)
                dblPoint = mmData / onePtTo_mm;

            return dblPoint;
        }


        ///---------------------------------------------------------------
        /// <summary>
        /// str を int にして返す
        /// </summary>
        /// <param name="str">int変換前、文字</param>
        /// <param name="defvalue">int初期値</param>
        /// <returns>str を int にした値</returns>
        ///---------------------------------------------------------------
        public int StrToInt(string str, int defvalue)
        {
            int itemp = defvalue;
            int.TryParse(str, out itemp);
            return itemp;
        }

        ///---------------------------------------------------------------
        /// <summary>
        /// 表示する項目タイトルの中から、最大文字数を返す
        /// </summary>
        /// <param name="playerInfo">各項目の存在と項目タイトル文字列取得用</param>
        /// <returns>表示する項目タイトルの中から、最大文字数</returns>
        ///---------------------------------------------------------------
        public int GetTileCharMaxCount(PlayerInfo playerInfo)
        {
            int maxCount = 6;   // default

            if (playerInfo.dispResultTitle.dispItem == true)
                if (playerInfo.dispResultTitle.Race.Length > maxCount)
                    maxCount = playerInfo.dispResultTitle.Race.Length;

            if (playerInfo.dispResultTitle.dispNumber == true)
            {
                if (playerInfo.dispResultTitle.BibNumber.Length > maxCount)
                    maxCount = playerInfo.dispResultTitle.BibNumber.Length;
            }
            if (playerInfo.dispResultTitle.dispName == true)
            {
                if (playerInfo.dispResultTitle.Name.Length > maxCount)
                    maxCount = playerInfo.dispResultTitle.Name.Length;
            }
            if (playerInfo.dispResultTitle.dispGrossTime == true)
            {
                if (playerInfo.dispResultTitle.GrossTime.Length > maxCount)
                    maxCount = playerInfo.dispResultTitle.GrossTime.Length;
            }
            if (playerInfo.dispResultTitle.dispNetTime == true)
            {
                if (playerInfo.dispResultTitle.NetTime.Length > maxCount)
                    maxCount = playerInfo.dispResultTitle.NetTime.Length;
            }
            if (playerInfo.dispResultTitle.dispPlace == true)
            {
                if (playerInfo.dispResultTitle.Place.Length > maxCount)
                    maxCount = playerInfo.dispResultTitle.Place.Length;
            }
            if (playerInfo.dispResultTitle.dispPlaceByAll == true) {
                if (playerInfo.dispResultTitle.PlaceByAll.Length > maxCount)
                    maxCount = playerInfo.dispResultTitle.PlaceByAll.Length;
            }

            return maxCount;

        }

        ///---------------------------------------------------------------
        /// <summary>
        /// 項目内表示文字列(itemStr)が １行表示文字最大数(playInfo.ItemCharsMax)を超えたとき
        /// 項目タイトルを２行の上側に表示するようにする
        /// </summary>
        /// <param name="pdfCntByte">PDF文字列出力処理用Object</param>
        /// <param name="playerInfo">FontSizeなど必要情報取得用Object</param>
        /// <param name="titleStr">項目タイトル表示文字列</param>
        /// <param name="itemStr">項目内容表示文字列</param>
        /// <param name="titleX">項目タイトル表示Y座標。取得。</param>
        /// <param name="titleY">項目タイトル表示Y座標。取得し、変更して返す。</param>
        /// <returns>なし</returns>
        ///---------------------------------------------------------------
        public void SetItemTitle(PdfContentByte pdfCntByte, PlayerInfo playerInfo, string titleStr, string itemStr, float titleX, ref float titleY)
        {
            float rowMargin = (float)mmToPoint(playerInfo.rowMargin);   // 行間
            float fntSizeFlt = playerInfo.dispResultTitle.fontSize;     // fontSize
            int itemCharsMax = playerInfo.ItemCharsMax;                // 項目内容1行表示最大文字数。これ超えたら改行

            // 最大数指定なし or 最大数以下のとき
            if ( (itemCharsMax <= 0) || (itemStr.Length <= itemCharsMax) )
            {
                pdfCntByte.ShowTextAligned(Element.ALIGN_LEFT, titleStr, titleX, titleY, 0);   // 項目名表示
            }
            // 項目内容が 2行のとき
            else
            {
                titleY = titleY + fntSizeFlt + rowMargin;   // 項目内容が 2行になるので、項目名も2行にする。
                pdfCntByte.ShowTextAligned(Element.ALIGN_LEFT, titleStr, titleX, titleY, 0);   // 項目名表示
            }

        }

        ///---------------------------------------------------------------
        /// <summary>
        /// 項目内容表示文字列(dispStr)が １行表示文字最大数(playInfo.ItemCharsMax)を超えたとき
        /// 最大数以降の文字列を2行目に表示する。
        /// </summary>
        /// <param name="pdfCntByte">PDF文字列出力処理用Object</param>
        /// <param name="playerInfo">FontSizeなど必要情報取得用Object</param>
        /// <param name="contY">項目内容表示Y座標。取得し、変更して、返す。</param>
        /// <param name="titleY">項目タイトル表示Y座標。取得し、変更して返す。</param>
        /// <returns>なし</returns>
        ///---------------------------------------------------------------
        public void SetItemAndCalcNextY(PdfContentByte pdfCntByte, string dispStr, PlayerInfo playerInfo, ref float contY, ref float titleY)
        {
            float contX = (float)mmToPoint(playerInfo.ItemContentsX);   // 項目内容用
            float rowMargin = (float)mmToPoint(playerInfo.rowMargin);   // 行間
            float fntSizeFlt = playerInfo.dispResultTitle.fontSize;     // fontSize
            int itemCharsMax = playerInfo.ItemCharsMax;                // 項目内容1行表示最大文字数。これ超えたら改行

            // 最大数指定なし or 最大数以下のとき
            if ( (itemCharsMax <= 0) || (dispStr.Length <= itemCharsMax) )
            {
                pdfCntByte.ShowTextAligned(Element.ALIGN_LEFT, dispStr, contX, contY, 0);
                titleY = titleY + fntSizeFlt + rowMargin;
                contY = contY + fntSizeFlt + rowMargin;
            }
            else
            {
                // 2行に分解
                int lg = dispStr.Length - itemCharsMax;
                string scndLineStr = dispStr.Substring(itemCharsMax, lg);
                pdfCntByte.ShowTextAligned(Element.ALIGN_LEFT, scndLineStr, contX, contY, 0);
                contY = contY + fntSizeFlt + rowMargin;

                string firstLineStr = dispStr.Substring(0, itemCharsMax);
                pdfCntByte.ShowTextAligned(Element.ALIGN_LEFT, firstLineStr, contX, contY, 0);
                titleY = titleY + fntSizeFlt + rowMargin;
                contY = contY + fntSizeFlt + rowMargin;
            }
            
            logger.Trace("dispPlace=true; dispString=" + dispStr);
        }

        
        ///---------------------------------------------------------------
        /// <summary>
        /// DayTime 情報(例: "2020/11/23 13:38:56 +12:00")から Time(例:"13:38:56") のみぬきだす。
        /// </summary>
        /// <param name="dayTimeStr">DayTime文字列</param>
        /// <returns>dayTimeStr から Time のみ抜き出した文字列 </returns>
        ///---------------------------------------------------------------
        public string GetTime(string dayTimeStr)
        {
            string retStr = "00:00:00";

            try
            {
                string[] paramsStr = dayTimeStr.Split(' ');
                if (paramsStr.Length >= 2)
                    retStr = paramsStr[1];
            }
            catch
            {
                retStr = "00:00:00";
            }

            if (retStr.Equals("00:00:00"))
                logger.Error("GetTime() get failure. value = " + dayTimeStr);

            return retStr;
        }

        ///---------------------------------------------------------------
        /// <summary>
        /// "hh:mm:ss.nnn" msecを四捨五入して "hh:mm:ss"で返す
        /// </summary>
        /// <param name="hhmmssnnnStr">"hh:mm:ss.nnn" </param>
        /// <returns>四捨五入した "hh:mm:ss"</returns>
        ///---------------------------------------------------------------
        public string RoundingMsec(string hhmmssnnnStr)
        {
            DateTime dt = DateTime.Parse(hhmmssnnnStr);
            dt = dt.AddMilliseconds(500.0);      // 500ms 四捨五入

            string retStr = string.Empty;
            // 0時、mm分、ss秒
            if (dt.Hour==0)
            {
                // 0時間、mm分、ss秒
                if (dt.Minute != 0)
                    retStr = dt.Minute.ToString() + ":" + dt.Second.ToString("#00");

                // 0時間、0分、ss秒
                else
                    retStr = dt.Second.ToString();
            }
            // hh時、mm分、ss秒
            //else if (dt.Minute == 0)
            else
                retStr = dt.Hour + ":" + dt.Minute.ToString("#00") + ":" + dt.Second.ToString("#00");

            return retStr;
        }

        ///---------------------------------------------------------------
        /// <summary>
        /// filenameの拡張子を参照し対応しているファイルか確認する
        /// </summary>
        /// <param name="filename">対象ファイル名</param>
        /// <returns>true:OK、false:NG </returns>
        ///---------------------------------------------------------------
        public bool CheckImageType(string filename)
        {
            int idx = filename.LastIndexOf(".");
            if (idx < 0)
            {
                return false;
            }

            string ext = filename.Substring(idx);
            if ((ext != ".jpg") && (ext != ".jpeg") && (ext != ".jpe") && (ext != ".pjpeg") &&
                (ext != ".jfif") && (ext != ".bmp") && (ext != ".gif") && (ext != ".png") &&
                (ext != ".JPG") && (ext != ".JPEG") && (ext != ".JPE") && (ext != ".PJPEG") &&
                (ext != ".JFIF") && (ext != ".BMP") && (ext != ".GIF") && (ext != ".PNG"))
            {
                return false;
            }

            return true;
        }
    }



}
