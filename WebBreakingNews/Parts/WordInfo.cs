﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


using System.IO;
using System.Xml.Linq;
using System.Text;


namespace WebBreakingNews.Parts
{
    ///---------------------------------------------------------------
    /// <summary>
    /// word XMLファイル処理クラス
    /// </summary>
    ///---------------------------------------------------------------
    public class WordInfo
    {
        private static WordInfo myObject;

        public static readonly string ENG = "Eng";
        public static readonly string JPN = "Jpn";
        public static string langKind { set; get; }

        public List<string> MsgErrJpnLists;
        public List<string> MsgErrEngLists;

        ///---------------------------------------------------------------
        /// <summary>
        /// 本クラスObject返す
        /// </summary>
        /// <param name="_langKind">日本語、英語指定</param>
        ///---------------------------------------------------------------
        public static WordInfo GetInstance(string _langKind)
        {
            if (myObject == null)
            {
                myObject = new WordInfo();
                langKind = _langKind;
                ReadWordInfoXmlFile();
            }
            return myObject;
        }

        ///---------------------------------------------------------------
        /// <summary>
        /// 本クラスObject返す
        /// </summary>
        ///---------------------------------------------------------------
        public static WordInfo GetInstance()
        {
            if (myObject == null)
            {
                myObject = new WordInfo();
                langKind = JPN;
                ReadWordInfoXmlFile();
            }
            return myObject;
        }

        ///---------------------------------------------------------------
        /// <summary>
        /// wordNoStr で指定された No に該当する MsgJpn/EngListの文字列を返す
        /// </summary>
        /// <param name="wordNoStr">ワード番号</param>
        /// <returns>wordNoStrに対応した文字列</returns>
        ///---------------------------------------------------------------
        public static string GetWord(string wordNoStr)
        {
            string retStr = "none";

            try
            {
                // "WD_xxxx" から 文字列取得
                int idx = wordNoStr.IndexOf("_");
                idx++;
                string noStr = wordNoStr.Substring(idx);
                int no = 0;
                int.TryParse(noStr, out no);
                if (langKind == JPN)
                {
                    retStr = myObject.MsgErrJpnLists[no];
                }
                else
                {
                    retStr = myObject.MsgErrEngLists[no];
                }

                if(retStr.Equals("none"))
                {
                    retStr = "Error." + "(" + no.ToString() + ")";
                }
            }
            catch {
                if (wordNoStr != null)
                    retStr = wordNoStr;
            }
            finally {
                
            }
            return retStr;
        }

        ///---------------------------------------------------------------
        /// <summary>
        ///   word XML ファイルから 情報を取得し  MsgJpn/EngList に保存
        /// </summary>
        /// <returns>なし</returns>
        ///---------------------------------------------------------------
        private static void ReadWordInfoXmlFile()
        {
            XName wordNoStr;
            //WordInfo wrdInfo = WordInfo.GetInstance();
            myObject.MsgErrJpnLists = new List<string>();
            myObject.MsgErrEngLists = new List<string>();
            // wordInfo List 一旦初期化
            for (int cnt = 0; cnt <= 9999; cnt++)
            {
                myObject.MsgErrJpnLists.Add("none");
                myObject.MsgErrEngLists.Add("none");
            }

            try
            {
                string wordInfoStrs = string.Empty;
                using (StreamReader sr = new StreamReader("wwwroot\\wordInfo_utf8.xml", Encoding.GetEncoding("UTF-8")))
                {
                    wordInfoStrs = sr.ReadToEnd();
                }
                if (wordInfoStrs.Length != 0)
                {
                    Stream xmlStreamData = new MemoryStream(System.Text.Encoding.Unicode.GetBytes(wordInfoStrs));
                    //Stream xmlStreamData = new MemoryStream(wordInfoStrs);
                    XElement xml = XElement.Load(xmlStreamData);

                    IEnumerable<XElement> wordItems = from item in xml.Elements("ErrorItem")
                                                      select item;
                    foreach (XElement info in wordItems)
                    {
                        for (int cnt = 0; cnt <= 9999; cnt++)
                        {
                            wordNoStr = "WDJ_" + cnt.ToString("#0000");
                            if (info.Element(wordNoStr) != null)
                            {
                                string tmpStr = info.Element(wordNoStr).Value.ToString();
                                myObject.MsgErrJpnLists[cnt] = tmpStr;
                            }
                            wordNoStr = "WDE_" + cnt.ToString("#0000");
                            if (info.Element(wordNoStr) != null)
                            {
                                string tmpStr = info.Element(wordNoStr).Value.ToString();
                                myObject.MsgErrEngLists[cnt] = tmpStr;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                string em = e.Message;

                NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
                logger.Error("main ReadWordInfoXmlFile() Error " + e.Message);
            }
        }
    }
}
