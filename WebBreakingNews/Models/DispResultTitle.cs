﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebBreakingNews.Models
{
    // 結果表示項目情報と項目文字列
    ///---------------------------------------------------------------
    /// <summary>
    /// 結果表示項目情報と項目文字列　保持クラス
    /// </summary>
    ///---------------------------------------------------------------
    public class DispResultTitle
    {
        // "種目,ナンバー,名前,グロスタイム,ネットタイム,順位,総合順位　表示/非表示情報"
        public int  dispMaxCount { get; set; }  // 表示項目数最大枠数
        public int  dispCount { get; set; }     // 表示項目数

        public bool dispEnglish { get; set; }   // JP/ENG
        public bool dispItem { get; set; }      // 種目
        public bool dispNumber { get; set; }    // ナンバー
        public bool dispName { get; set; }      // 名前
        public bool dispGrossTime { get; set; } // グロスタイム
        public bool dispNetTime { get; set; }   // ネットタイム
        public bool dispPlace { get; set; }     // 順位
        public bool dispPlaceByAll { get; set; }    // 総合順位
        
        
        public string fontKind { get; set; }    // Font種類
        public float fontSize { get; set; }     // FontSize (pt)


        public string Race { get; set; }        // 種目名Title表示文字
        public string BibNumber { get; set; }   // ナンバーTitle表示文字
        public string Name { get; set; }        // 氏名Title表示文字
        public string GrossTime { get; set; }   // グロスタイムTitle表示文字
        public string NetTime { get; set; }     //　ネットTitle表示文字
        public string Place { get; set; }       // 順位Title表示文字
        public string PlaceByAll { get; set; }  // 総合順位表示文字
    }

}
