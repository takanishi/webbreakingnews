﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace WebBreakingNews.Models
{
    ///---------------------------------------------------------------
    /// <summary>
    /// 大会マスタ (M_EVENT) 用クラス
    /// </summary>
    ///---------------------------------------------------------------
    //[Serializable]
    public class Taikai
    {
        [System.ComponentModel.DataAnnotations.Key]
        public string EventCode { get; set; }   // 大会コード

        public string EventName { get; set; }   // 大会名

        public bool DelFlg { get; set; }    // TRUE:WEB速報とWEB完走証の公開を停止する。FALSE:WEB速報とWEB完走証の公開する。

        //public int MyDataBaseId{ get; set;}

        public string DataBaseName { get; set; }    // DB名
    }

}
