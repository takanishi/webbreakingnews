﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// Add
using System.Xml.Serialization;

namespace WebBreakingNews.Models
{

    ///---------------------------------------------------------------
    /// <summary>
    /// DB M_CERTIFICATION の PRINT_PSIOTION1 の Xmlファイルデータ用
    /// データ保持クラス。
    /// </summary>
    ///---------------------------------------------------------------
    [XmlRoot("PrintSettings")]
    public class PrintSettings
    {
        [XmlElement("English")]
        public string English { get; set; }


        // フォントの種類
        [XmlElement("FontType")]
        public string FontType { get; set; }

        // フォントサイズ
        [XmlElement("FontSize")]
        public string FontSize { get; set; }

        // 縦位置タイトル
        [XmlElement("VerticalPositionTitle")]
        public string VerticalPositionTitle { get; set; }

        // 縦位置本文
        [XmlElement("VerticalPositionText")]
        public string VerticalPositionText { get; set; }

        // 横位置
        [XmlElement("HorizontalPosition")]
        public string HorizontalPosition { get; set; }

        // 行間
        [XmlElement("LineSpace")]
        public string LineSpace { get; set; }


        // タイトルX位置
        [XmlElement("ItemTitleX")]
        public string ItemTitleX { get; set; }

        // タイトルY位置
        [XmlElement("ItemTitleY")]
        public string ItemTitleY { get; set; }

        // 本文X位置
        [XmlElement("ItemContentsX")]
        public string ItemContentsX { get; set; }

        // 本文Y位置
        [XmlElement("ItemContentsY")]
        public string ItemContentsY { get; set; }


        // イメージファイル枠 縦
        [XmlElement("ImageHeight")]
        public string ImageFrameHeight { get; set; }

        // イメージファイル枠 幅
        [XmlElement("ImageWidth")]
        public string ImageFrameWidth { get; set; }


        // イメージファイルX位置
        [XmlElement("ImageX")]
        public string ImageX { get; set; }

        // イメージファイルX位置
        [XmlElement("ImageY")]
        public string ImageY { get; set; }

        // 列間
        [XmlElement("BetweenRow")]
        public string BetweenRow { get; set; }

        // 倍率
        [XmlElement("ImageMagnifi")]
        public string ImageMagnifi { get; set; }

        // 折り返し文字数
        [XmlElement("ItemCharsMax")]
        public string ItemCharsMax { get; set; }

        // 項目
        [XmlElement("RaceJP")]
        public string RaceJP { get; set; }

        // ナンバー
        [XmlElement("BibNumberJP")]
        public string BibNumberJP { get; set; }

        // 氏名
        [XmlElement("NameJP")]
        public string NameJP { get; set; }

        // 記録
        [XmlElement("GrossTimeJP")]
        public string GrossTimeJP { get; set; }

        // ネットタイム
        [XmlElement("NetTimeJP")]
        public string NetTimeJP { get; set; }

        // 順位
        [XmlElement("PlaceJP")]
        public string PlaceJP { get; set; }

        // PlaceByAllJP
        [XmlElement("PlaceByAllJP")]
        public string PlaceByAllJP { get; set; }



        // 項目
        [XmlElement("RaceENG")]
        public string RaceENG { get; set; }

        // ナンバー
        [XmlElement("BibNumberENG")]
        public string BibNumberENG { get; set; }

        // 氏名
        [XmlElement("NameENG")]
        public string NameENG { get; set; }

        // 記録
        [XmlElement("GrossTimeENG")]
        public string GrossTimeENG { get; set; }

        // ネットタイム
        [XmlElement("NetTimeENG")]
        public string NetTimeENG { get; set; }

        // 順位
        [XmlElement("PlaceENG")]
        public string PlaceENG { get; set; }

        // PlaceByAllJP
        [XmlElement("PlaceByAllENG")]
        public string PlaceByAllENG { get; set; }
    }
}
