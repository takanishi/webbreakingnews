﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebBreakingNews.Models
{
    ///---------------------------------------------------------------
    /// <summary>
    /// 選手情報保持クラス (種目、選手、リザルト、完走証　Table情報)
    /// </summary>
    ///---------------------------------------------------------------
    public class PlayerInfo
    {
        ///---------------------------------------------------------------
        /// <summary>
        /// コンストラクタ
        /// </summary>
        ///---------------------------------------------------------------
        public PlayerInfo()
        {
            REFERENCE_RECORD = "※上記記録は速報値です。";

            PlayerName = "";
            PlayerNameKana = "";
            Gender = ""; // 性別
            Age = -1;
            TodoufukenName = "";
            TownName = "";
            TeamName = "";
            SequenceNo = -1;
            Laps = -1;
            GrossTime = "";
            NetTime = "";
            ItemNumber = -1;
            ItemName = "";
            Place = "";
            PlaceByAll = "";

            // set default
            ImgFrameHeight = 50; // mm
            ImgFrameWidth = 50;  //mm
            ImgXmm = 102.3;      // mm 約290pt
            ImgYmm = 77.61;      // mm 約220pt
 
            ItemTitleX = 7.06;      // mm 約20pt
            ItemTitleY = 141.11;    // mm 約400pt
            ItemContentsX = 50;        // 50mm
            ItemContentsY = 141.11;    // mm 約400pt
            rowMargin = 5;      // mm
            ItemCharsMax = -1;

            dispResultTitle = new DispResultTitle();
            dispResultTitle.fontKind = "メイリオ";
            dispResultTitle.fontSize = 14;
            dispResultTitle.dispMaxCount = 7;       // 表示項目最大数。
            dispResultTitle.dispCount = 0;
            dispResultTitle.dispEnglish = false;
            dispResultTitle.dispItem = false;
            dispResultTitle.dispNumber = false;
            dispResultTitle.dispName = false;
            dispResultTitle.dispGrossTime = false;
            dispResultTitle.dispNetTime = false;
            dispResultTitle.dispPlace = false;
            dispResultTitle.dispPlaceByAll = false;
        }

        //-----------------------------------------------------------------------------
        // 以下 完走証 Table メンバ
        //-----------------------------------------------------------------------------
        public int ID { get; set; }

        //public string ITEM_NAME { get; set; }   // WEB速報/WEB完走証の項目名
        
        //public Boolean RANKING_DISPLAY_FLG { get; set; }    // TRUE:WEB速報とWEB完走証に順位を表示する。FALSE:WEB速報とWEB完走証に順位を表示しない。

        public byte[] PDF { get; set; } // 完走証 ベース Pdfファイル Binaryデータ
        public string REFERENCE_RECORD { get; set; }    // 「※上記記録は速報値です。」を固定表示する。

        public string PRINT_POSITION1 { get; set; }     // 画像、文字、X/Y座標 XML


        //-----------------------------------------------------------------------------
        //　以下 完走証　Table メンバ解析後、抽出した情報
        //-----------------------------------------------------------------------------
        public double ImgFrameHeight { get; set; }  // 画像枠 縦(mm)
        public double ImgFrameWidth { get; set; }   // 画像枠 幅(mm)
        public double ImgXmm { get; set; }          // 画像 X座標(mm)
        public double ImgYmm { get; set; }          // 画像 Y座標(mm)
        
        public double ImageMagnifi { get; set; }    // 画像倍率(%)

        public double ItemTitleX { get; set; }          // 項目名 開始 X座標(mm)
        public double ItemTitleY { get; set; }          // 項目名 開始 Y座標(mm)
        public double ItemContentsX { get; set; }       // 項目内容 開始 X座標(mm)
        public double ItemContentsY { get; set; }       // 項目内容 開始 Y座標(mm)

        public int ItemCharsMax { get; set; }           // 項目内容 1行文字数。 -1は改行しない。


        //public double columMargin { get; set; }     // 文字列間値(mm)
        public double rowMargin { get; set; }       // 文字列行間(mm)

        public DispResultTitle dispResultTitle { get; set; }    // 結果表示項目情報と項目文字列


        //-----------------------------------------------------------------------------
        // 共通情報
        //-----------------------------------------------------------------------------
        public int BibNumber { get; set; }          // ナンバー(ゼッケン)

        public int EventNumber { get; set; }        //  大会コード

        public string EventName { get; set; }       // 大会名


        //-----------------------------------------------------------------------------
        // 以下 選手/リザルト/種目 Table から取得した情報
        //-----------------------------------------------------------------------------
        public string PlayerName { get; set; }      // 氏名
        public string PlayerNameKana { get; set; }  // 氏名(かな)
        public string Gender { get; set; }          // 性別
        public int Age { get; set; }                // 年齢
        public string TodoufukenName { get; set; }  // 都道府県
        public string TownName { get; set; }        // 町
        public string TeamName { get; set; }        // チーム名


        public int SequenceNo { get; set; }
        public int Laps { get; set; }               // 周回
        public string GrossTime { get; set; }       // グロスタイム
        public string NetTime { get;set; }          // ネットタイム


        public int ItemNumber { get; set; }         // 種番
        public string ItemName { get; set; }        // 種目


        public string Place { get; set; }           // 順位
        public string PlaceByAll { get; set; }      // 総合順位
    }

}
