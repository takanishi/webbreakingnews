﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


// Add
using WebBreakingNews.Data;
using Microsoft.EntityFrameworkCore;    // Add for UseInMemoryDatabase. used NuGetMicrosoft.EntityFrameworkCore.InMemory v5.0.2
using Microsoft.AspNetCore.Http;        // Add for session

using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Mvc;         // Add for RequireHttpsAttribute
using Microsoft.AspNetCore.Authentication.Cookies;  // NuGet Microsoft.AspNetCore.Authentication
using Microsoft.AspNetCore.CookiePolicy;            //

namespace WebBreakingNews
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        public IConfiguration Configuration { get; }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            //services.AddRazorPages(options =>
            //{
            //    //options.
            //    options.Conventions.
            //}

   

            ////************************************************************************
            //// Https の場合、以下を有効にする。
            //// ただし、VSでデバッグする場合は、IIS側を停止しないと起動できない。
            //services.AddMvc(options =>
            //{
            //    options.SslPort = 44344;
            //    options.Filters.Add(new RequireHttpsAttribute());
            //});


            // Add use session
            //services.AddSession();
            // Add: use session name / timeout then
            services.AddSession(options =>
            {
                options.Cookie.SameSite = SameSiteMode.Lax;       // Add 2021.03.09 (default is Lax)
                // Lax    : クライアントが「同じサイト」のリクエストと「クロスサイト」のトップレベルのナビゲーションでCookieを送信する必要があることを示します。
                // None   : クライアントが同じサイトの制限を無効にする必要があることを示します。
                // Strict : クライアントが「同じサイト」のリクエストでのみCookieを送信する必要があることを示します。
                // Unspecified : SameSiteフィールドは設定されません。クライアントは、デフォルトのCookieポリシーに従う必要があります。
                options.Cookie.Name = "ASP.NET_SessionId";        // Sessionで使用するクッキーの名前(SessionIDを保管するもの名前)
                options.IdleTimeout = TimeSpan.FromMinutes(30);    // Sessionが切れるまでのタイムアウト時間 (.FromDaysや .FromSecondsなどある)
                //options.IdleTimeout = TimeSpan.FromSeconds(8);  // for deb
                options.Cookie.IsEssential = true;                // Cookie を必要とするか?(true:する)
                options.Cookie.SecurePolicy = CookieSecurePolicy.None;  // Always is Https?
            }
            );

            //// Add 2021.03.09
            //services.Configure<CookiePolicyOptions>(options =>
            //{
            //    //options.CheckConsentNeeded = context => !context.User.Identity.IsAuthenticated;  / User認証したいとき
            //    //options.MinimumSameSitePolicy = SameSiteMode.None;
            //
            //    // セッションハイジャック等を防ぐために httpOnly属性（JSから触れなくする）、
            //    // Secure属性（SSL時のみクッキーを送信）を設定できる
            //    // Configure(...)に　 "app.UseCookiePolicy();"　が必要
            //    //options.HttpOnly = HttpOnlyPolicy.Always;       // Detal.cshtml に javascript いれても動いた (default is none)
            //    //options.Secure = CookieSecurePolicy.Always;
            //});

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                //app.UseDeveloperExceptionPage();
                app.UseStatusCodePagesWithReExecute("/Error", "?errno={0}");
            }
            else
            {
                //app.UseExceptionHandler("/Error");
                app.UseStatusCodePagesWithReExecute("/Error", "?errno={0}");

                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            // Add
            app.UseSession();

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            //// Add ( None だと 起動しなかった。 Always だと 
            ///        このCookieは JavaScriptdocument.cookieAPI にアクセスできません。 になるのではずした。
            app.UseCookiePolicy();  // for options.HttpOnly = HttpOnlyPolicy.Always;

            //app.UseCookiePolicy(new CookiePolicyOptions
            //{
            //    // HttpOnly = HttpOnlyPolicy.Always
            //
            //    // Always:1	
            //    //     The cookie is configured with a HttpOnly attribute. 
            //    //     This cookie inaccessible to the JavaScript document.cookie API.
            //    // None:0
            //    //     The cookie does not have a configured HttpOnly behavior. 
            //    //      This cookie can be accessed by JavaScript document.cookie API.
            //    HttpOnly = Microsoft.AspNetCore.CookiePolicy.HttpOnlyPolicy.Always
            //});
            ////app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
            });
        }
    }
}
