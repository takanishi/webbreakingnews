﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

// Add
using WebBreakingNews.Models;
using WebBreakingNews.Data;
using NLog.Web;


namespace WebBreakingNews.Pages
{
    ///---------------------------------------------------------------
    /// <summary>
    /// エラー ページクラス
    /// </summary>
    ///---------------------------------------------------------------
    public class ErrorMsgModel : PageModel
    {
        // NLog用
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        //[TempData]
        //public string errorMessage { get; set; }

        [BindProperty]
        public string[] ErrMsgs { get; set; }


        ///---------------------------------------------------------------
        /// <summary>
        /// OnGet()処理。 送られてきた エラーメッセージを errMsg に保存
        /// </summary>
        /// <param name="errtitle">ワード番号</param>
        /// <param name="errmsg">ワード番号</param>
        /// <returns>wordNoStrに対応した文字列</returns>
        ///---------------------------------------------------------------
        //public void OnGet(string errtitle, string errmsg)
        public IActionResult OnGet(string errtitle, string errmsg)
        {
            string sessionIdStr = HttpContext.Session.Id;
            logger.Error("ErrorMsg:OnGet() errmsg=" + errmsg + " SessionID=" + sessionIdStr);

            //errMsg = errmsg;
            if (errmsg.Length <= 0)
                errmsg = "Error.";

            ErrMsgs = errmsg.Split('\n');


            //if(errMsg==null)
            //    return RedirectToPage("/Index");
            //else 
            //    return Page();
            return Page();
        }

        ///---------------------------------------------------------------
        /// <summary>
        /// Top(Index) ページに戻す
        /// </summary>
        ///---------------------------------------------------------------
        public IActionResult OnPost()
        {
            return RedirectToPage("/Index");
        }

    }

}
