﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


// Add
using System.IO;
using System.Data.SqlClient;                // NuGet で Microsoft.Data.SqlClient v2.1.1 インストール
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;   // for SelectListItem
using WebBreakingNews.Models;
using WebBreakingNews.Data;
using WebBreakingNews.Parts;
using NLog.Web;
using Microsoft.AspNetCore.Http;            // for session
using System.Runtime.Serialization.Formatters.Binary; // for BinaryFormatter
using System.Text.Json;                     // for JsonSerializer



namespace WebBreakingNews.Pages
{
    ///---------------------------------------------------------------
    /// <summary>
    /// Index ページクラス
    /// </summary>
    ///---------------------------------------------------------------
    // @Html.AntiForgeryToken()が、CSRF対策のワンタイムトークン(hidden input)を生成し、
    //  ValidateAntiForgeryToken属性が付与されたPostActionが、アクション実行時にトークンの検証を行う
    [ValidateAntiForgeryToken]

    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;

        // NLog用
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger(); 


        [BindProperty]
        // 選手ナンバー
        public String mNumber { get; set; }

        // DB名 List
        List<string> myDataBaseNames;

        // 大会名/Code リスト
        List<Taikai> Taikais;


        ///---------------------------------------------------------------
        /// <summary>
        /// コンストラクタ
        /// </summary>
        ///---------------------------------------------------------------
        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;                   // 現在未使用
        }
        
        ///---------------------------------------------------------------
        /// <summary>
        ///   SQLサーバーからDB名一覧取得し、DBごとの大会Table情報を取得。
        /// </summary>
        ///---------------------------------------------------------------
        //public void OnGet()
        //public async Task OnGetAsync()
        public async Task<IActionResult> OnGetAsync()
        {
            //this.HttpContext.Response.Cookies.Append("Key_T", "Value_T"); // cookie に保存する場合の例

            //----------------------------------------------------------------
            //  サーバから DB名取得 し、DB名List作成(context.MyDataBase) 
            //----------------------------------------------------------------
            int seqno = 1;      // エラー時、ソースがどこまで進んだか判断用
            string sessionIdStr = HttpContext.Session.Id;
            logger.Info("Index:OnGetAsync() seqno = " + seqno.ToString() + " SessionID=" + sessionIdStr);

            try
            {
                // SQL Server へ接続
                seqno = 2;
                logger.Trace("Index:OnGetAsync() seqno = " + seqno.ToString() + " SessionID=" + sessionIdStr);
                Microsoft.Data.SqlClient.SqlConnection cont = null;
                using (cont = new Microsoft.Data.SqlClient.SqlConnection(SqlConnectionString.Instance.master))
                {
                    // SQL Server から データベースのリストを取得。→ dBNameList[]
                    await cont.OpenAsync();
                    using (Microsoft.Data.SqlClient.SqlCommand command = new Microsoft.Data.SqlClient.SqlCommand())
                    {
                        seqno = 3;
                        logger.Trace("Index:OnGetAsync() seqno = " + seqno.ToString() + " SessionID=" + sessionIdStr);
                        command.Connection = cont;
                        command.CommandText = "SELECT name from sys.databases WHERE name <> 'master' AND name <> 'tempdb' AND name <> 'model' AND name <> 'msdb';";
                        command.ExecuteNonQuery();
                        using (Microsoft.Data.SqlClient.SqlDataReader reader = await command.ExecuteReaderAsync())
                        {
                            seqno = 4;
                            logger.Trace("Index:OnGetAsync() seqno = " + seqno.ToString() + " SessionID=" + sessionIdStr);
                            myDataBaseNames = new List<string>();
                            while (await reader.ReadAsync())
                            {
                                seqno = 5;
                                logger.Trace("Index:OnGetAsync() seqno = " + seqno.ToString() + " SessionID=" + sessionIdStr);
                                string tmpDbName = reader.GetString(0);
                                    myDataBaseNames.Add(tmpDbName);
                            }
                        }
                        //await reader.CloseAsync(); using で Openしているのでいらない
                    }
                    await cont.CloseAsync();
                }
            }
            catch(Exception e)
            {
                string errTitle = "エラー";
                // データサーバーへのアクセスに失敗しました。
                string errMsg = WordInfo.GetWord("WD_0010");
                logger.Error(errMsg + " " + e.Message + " OnGet:[" + seqno.ToString() + "]" + " SessionID=" + sessionIdStr);

                return RedirectToPage("/ErrorMsg", new { errtitle= errTitle, errmsg= errMsg });
            }


            //----------------------------------------------------------------
            // 各DB の M_EVENT(大会名)Table　から 大会名と大会Code取得
            //----------------------------------------------------------------
            try
            {
                Taikais = new List<Taikai>();
                seqno = 6;
                logger.Trace("Index:OnGetAsync() seqno = " + seqno.ToString() + " SessionID=" + sessionIdStr);

                foreach (var dbName in myDataBaseNames)
                {
                    // SQL Server へ接続
                    Microsoft.Data.SqlClient.SqlConnection cont = null;
                    var sqlCont = SqlConnectionString.Instance.GetSqlConnection(dbName);
                    using (cont = new Microsoft.Data.SqlClient.SqlConnection(sqlCont))
                    {
                        // SQL Server へ SQLコマンド送信
                        seqno = 7;
                        logger.Trace("Index:OnGetAsync() seqno = " + seqno.ToString() + " SessionID=" + sessionIdStr);
                        await cont.OpenAsync();
                        using (Microsoft.Data.SqlClient.SqlCommand command = new Microsoft.Data.SqlClient.SqlCommand())
                        {
                            seqno = 8;
                            logger.Trace("Index:OnGetAsync() seqno = " + seqno.ToString() + " SessionID=" + sessionIdStr);
                            command.Connection = cont;
                            command.CommandText = "select * from M_EVENT";
                            try
                            {
                                command.ExecuteNonQuery();
                            }
                            catch
                            {
                                continue;   // DBに期待する Table が無いなど、何かエラーが起きたら次のDBへ
                            }

                            // Read
                            using (Microsoft.Data.SqlClient.SqlDataReader reader = await command.ExecuteReaderAsync())
                            {
                                while (await reader.ReadAsync())
                                {
                                    try
                                    {
                                        bool del_flg = Convert.ToBoolean(reader["DEL_FLG"].ToString());
                                        if (del_flg == false) // Web速報許可(=false)
                                        {
                                            Taikai taikai = new Taikai();
                                            taikai.EventCode = reader["EVENT_CODE"].ToString();  // TaikaiCode
                                            taikai.EventName = reader["EVENT_NAME"].ToString();  // TaikaiName
                                            taikai.DelFlg = del_flg;                             // DelFlg
                                            taikai.DataBaseName = dbName;
                                            Taikais.Add(taikai);
                                            seqno = 9;
                                            string taikaiPrms = dbName + "," + taikai.EventCode + "," + taikai.EventName;
                                            logger.Trace("Index:OnGetAsync() seqno = " + seqno.ToString() + " SessionID=" + sessionIdStr + "Db,EvCd,EvName="+ taikaiPrms);
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        string name = e.Message;
                                        // 予定パラメータが無い場合は、reader の whileをやめる
                                        if ((name.Equals("EVENT_CODE")) || (name.Equals("EVENT_NAME")) || (name.Equals("DEL_FLG")))
                                            break;
                                    }
                                }
                            }
                        }
                        await cont.CloseAsync();
                    }
                } // end of foreach   
            }
            catch(Exception e)
            {
                string errTitle = "エラー";
                //string errMsg = "登録に無いナンバーです。\n"+
                //              "ナンバーをお確かめの上、もう一度検索をしてください。\n" +
                //                "※当日エントリー内容を健康した場合、速報が閲覧できない場合がございます。";
                string errMsg = WordInfo.GetWord("WD_0001") ;
                logger.Error(errMsg + " " + e.Message + " OnGet:[" + seqno.ToString() + "]" + " SessionID=" + sessionIdStr);

                return RedirectToPage("/ErrorMsg", new { errtitle = errTitle, errmsg = errMsg });
            }

            //----------------------------------------------------------------
            //  DropDownList に大会名 登録
            //----------------------------------------------------------------
            seqno = 10;
            logger.Trace("Index:OnGetAsync() seqno = " + seqno.ToString() + " SessionID=" + sessionIdStr);
            List<SelectListItem> items = new List<SelectListItem>();
            foreach (var tbl in Taikais)
            {
                items.Add(new SelectListItem() { Value = tbl.EventCode, Text = tbl.EventName });    // Html.DropDownList then
                //TaikaiList.Add(new SelectListItem() { Value = tbl.EventCode, Text = tbl.EventName });  // for select tag then

                ViewData["DropDownListItems"] = items;
                
            }
            // 1大会もデータが無い。
            if (Taikais.Count == 0)
            {
                Taikai taikai = new Taikai();
                taikai.EventCode = "-1";    // TaikaiCode
                taikai.EventName = "none.";  // TaikaiName
                taikai.DelFlg = true;       // DelFlg
                taikai.DataBaseName = "none.";
                Taikais.Add(taikai);
                string errMsg = WordInfo.GetWord("WD_0012");    // 大会リストがありません。
                items.Add(new SelectListItem() { Value = "-1", Text = errMsg });
                ViewData["DropDownListItems"] = items;
            }

            seqno = 11;
            logger.Trace("Index:OnGetAsync() seqno = " + seqno.ToString() + " SessionID=" + sessionIdStr);

            // Session に Taikais を保存
            HttpContext.Session.Set("TaikaiList", Taikais);
            
            
            //return RedirectToPage("/Index");
            return Page();

        } // end of OnGetAsync



        ///---------------------------------------------------------------
        /// <summary>
        ///  検索ボタン押されたときに来る。
        ///   ナンバー、大会名等を Session に保存し、DetailPageへ遷移
        /// </summary>
        ///---------------------------------------------------------------
        //public async Task<IActionResult> OnPostAsync()
        //public void OnPost()
        public IActionResult OnPost()
        {
            string sessionIdStr = HttpContext.Session.Id;
            logger.Info("Index:OnPost()" + " SessionID=" + sessionIdStr);
            string selectEventCode = Request.Form["DropDownListItems"];

            // 大会名は選択されたか
            if (selectEventCode.Length <=0)
            {
                logger.Error("Index:OnPost() Not selected taikai name." + " SessionID=" + sessionIdStr);
                //大会名は必須選択です。
                string errMsg = WordInfo.GetWord("WD_0006");
                return RedirectToPage("/ErrorMsg", new { errtitle = "エラー", errmsg = errMsg });
            }

            // ナンバー入力されたか
            if(mNumber == null)
            {
                logger.Error("Index:OnPost() Not input BibNumber." + " SessionID=" + sessionIdStr);
                //ゼッケンナンバーは必須入力です。
                string errMsg = WordInfo.GetWord("WD_0007");
                return RedirectToPage("/ErrorMsg", new { errtitle = "エラー", errmsg = errMsg });
            }

            // Session から Taikais を読み出し
            Taikais = new List<Taikai>();
            var session_taikais = HttpContext.Session.Get<List<Taikai>>("TaikaiList");
            Taikais = session_taikais;
            if (session_taikais == null)
            {
                logger.Error("Index:OnPost() Taikais is null　by session timeout.");
                //結果保持時間が経過しました。はじめから操作をやり直してください。
                string errMsg = WordInfo.GetWord("WD_0005");
                return RedirectToPage("/ErrorMsg", new { errtitle = "エラー", errmsg = errMsg });
            }

            // DropDownList で選択された、EventName に対応した EventCode と 一致する
            // DB名、大会名、大会コード、DelFlg　を取得
            bool find_f = false;
            string evcode = string.Empty;
            string evname = string.Empty;
            string dbname = string.Empty;
            string delflg = string.Empty;
             foreach (var item in Taikais)
            {
                evcode = item.EventCode;
                evname = item.EventName;
                dbname = item.DataBaseName;
                delflg = item.DelFlg.ToString();
                if (evcode == selectEventCode)
                {
                    find_f = true;
                    break;
                }
            }

            // 選択された大会名があった
            if ( (find_f == true)&&(mNumber!=null) )
            {
                //// Web速報の公開が許可されてない
                //if (delflg.Equals("True"))
                //{
                //    string errTitle = "エラー";
                //    // 選択された大会は、Web速報の公開が許可されていません。
                //    string errMsg = WordInfo.GetWord("WD_0002");
                //    logger.Error(errMsg + " SessionID=" + sessionIdStr);
                //
                //    return RedirectToPage("/ErrorMsg", new { errtitle = errTitle, errmsg = errMsg });
                //}

                // 大会リストがない。
                if (evcode.Equals("-1"))
                {
                    string errTitle = "エラー";
                    string errMsg = WordInfo.GetWord("WD_0012");    // 大会リストがありません。
                    logger.Error(errMsg + " SessionID=" + sessionIdStr);
                    
                    return RedirectToPage("/ErrorMsg", new { errtitle = errTitle, errmsg = errMsg });
                }
                else
                {
                    // Session に保存。DetailPage へ渡す
                    HttpContext.Session.SetString("dbName", dbname);
                    HttpContext.Session.SetString("EventName", evname);
                    HttpContext.Session.SetString("EventCode", evcode); ;
                    HttpContext.Session.SetString("DelFlg", delflg);
                    HttpContext.Session.SetString("PlayerNo", mNumber);

                    string msg = dbname + "," + evname + "," + evcode + "," + delflg + "," + mNumber;
                    logger.Info("OnPost() set selectedDBInfo=" + msg + " SessionID=" + sessionIdStr);

                    return RedirectToPage("/Detail");
                }
            }
            // 選択された大会名がなかった
            else
            {
                string errTitle = "エラー";
                // "登録に無いナンバーです。\n"+
　　            // "ナンバーをお確かめの上、もう一度検索をしてください。\n" +
                // "※当日エントリー内容を健康した場合、速報が閲覧できない場合がございます。"
                string errMsg = WordInfo.GetWord("WD_0001");
                logger.Error(errMsg + " SessionID=" + sessionIdStr);

                return RedirectToPage("/ErrorMsg", new { errtitle = errTitle, errmsg = errMsg });
            }
        } // end of OnPost

    }

    ///---------------------------------------------------------------
    /// <summary>
    ///  Session に Object 保存するための Method
    ///      セッション変数にClassやListなどのオブジェクトを保持したい場合、
    ///      Session拡張メソッドを使うとシンプルなソースコードになる。 とのこと。
    ///      Session拡張メソッドを、ジェネリック指定でjson形式にシリアライズし保持する。
    /// </summary>
    ///---------------------------------------------------------------
    public static class SessionExtensions
    {
        public static void Set<T>(this ISession session, string key, T value)
        {
            session.SetString(key, JsonSerializer.Serialize(value));
        }

        public static T Get<T>(this ISession session, string key)
        {
            var value = session.GetString(key);
            return value == null ? default : JsonSerializer.Deserialize<T>(value);
        }
    }


}
