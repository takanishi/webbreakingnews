using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;


// Add
using System.IO;
using WebBreakingNews.Models;
using WebBreakingNews.Data;
using WebBreakingNews.Parts;
using NLog.Web;
using Microsoft.AspNetCore.Http;        // Add for session


namespace WebBreakingNews.Pages
{
    ///---------------------------------------------------------------
    /// <summary>
    /// Detail ページクラス
    /// </summary>
    ///---------------------------------------------------------------
    // @Html.AntiForgeryToken()が、CSRF対策のワンタイムトークン(hidden input)を生成し、
    //  ValidateAntiForgeryToken属性が付与されたPostActionが、アクション実行時にトークンの検証を行う
    [ValidateAntiForgeryToken]
    //[RequestFormLimits(MultipartBodyLengthLimit = 56*1024*1024)]  // HttpPostの最大Byte数     // default 28MB

    public class DetailModel : PageModel
    {
        // NLog用
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        [BindProperty]
        public PlayerInfo mDspRslt { get; set; }   // GUI 結果表示用

        [BindProperty]
        public Microsoft.AspNetCore.Http.IFormFile PostedFile { get; set; }


        ///---------------------------------------------------------------
        /// <summary>
        /// OnGet 処理。 Detail Page 表示時など
        /// </summary>
        ///---------------------------------------------------------------
        //public void OnGet()
        //public async Task OnGetAsync()
        public async Task<IActionResult> OnGetAsync()
        {
            int seqno = 1;
            string sessionIdStr = HttpContext.Session.Id;
            logger.Info("Detail:OnGetAsync() seqno = " + seqno.ToString() + " SessionID=" + sessionIdStr);
            PlayerInfo playerInfo = new PlayerInfo();    // 完走証テーブル情報 仮保存用。
            MyParts myParts = new MyParts();

            // Top Page からの情報取得
            string[] topPageInfo;
            topPageInfo = new string[5];
            topPageInfo[0] = HttpContext.Session.GetString("dbName");
            topPageInfo[1] = HttpContext.Session.GetString("EventName");
            topPageInfo[2] = HttpContext.Session.GetString("EventCode");
            topPageInfo[3] = HttpContext.Session.GetString("DelFlg");
            topPageInfo[4] = HttpContext.Session.GetString("PlayerNo");
            if (topPageInfo[0] == null) // Session 時間切れのときは最初に戻る
            {
                logger.Error("DetailPage OnGetAsync() get NG. topPageInfo[0] is null." + " SessionID=" + sessionIdStr);
                //return RedirectToPage("/Index");
                //結果保持時間が経過しました。はじめから操作をやり直してください。
                string errMsg = WordInfo.GetWord("WD_0005");
                return RedirectToPage("/ErrorMsg", new { errtitle = "エラー", errmsg = errMsg });
            }
            else
            {
                string msg = "DetailPage OnGetAsync()" + " SessionID=" + sessionIdStr;
                logger.Trace(msg);
                int i = 0;
                foreach (var item in topPageInfo)
                {
                    logger.Trace("GetSession["+i.ToString() + "]=" + item);
                    i++;
                }
            }
           
            // TopPage からの DB名 設定
            string dbName = topPageInfo[0];

            //　TopPage からの ナンバ　設定
            int bibNumber = 0;
            int.TryParse(topPageInfo[4], out bibNumber);
            playerInfo.BibNumber = bibNumber;

            // TopPage から 大会名、大会Code 取得
            int eventNumber = 0;
            int.TryParse(topPageInfo[2], out eventNumber);
            playerInfo.EventNumber = eventNumber;
            playerInfo.EventName = topPageInfo[1];


            //----------------------------------------------------------------
            //  ナンバー と大会コード から　結果情報取得
            //----------------------------------------------------------------
            bool find_f = false;    // 結果見つかったら true
            try
            {
                // SQL Server へ接続
                Microsoft.Data.SqlClient.SqlConnection cont = null;
                var sqlCont = SqlConnectionString.Instance.GetSqlConnection(dbName);
                using (cont = new Microsoft.Data.SqlClient.SqlConnection(sqlCont))
                {
                    // SQL Server から 
                    seqno = 2;
                    logger.Trace("Detail:OnGetAsync() seqno = " + seqno.ToString() + " SessionID=" + sessionIdStr);
                    await cont.OpenAsync();
                    using (Microsoft.Data.SqlClient.SqlCommand command = new Microsoft.Data.SqlClient.SqlCommand())
                    {
                        seqno = 3;
                        logger.Trace("Detail:OnGetAsync() seqno = " + seqno.ToString() + " SessionID=" + sessionIdStr);
                        command.Connection = cont;
                        //command.CommandText = "SELECT * FROM T_RESULT WHERE T_RESULT.NUMBER_CARD = @param1";
                        command.CommandText =
                        "SELECT* FROM M_ITEM INNER JOIN M_PLAYER ON M_ITEM.ITEM_NUMBER = M_PLAYER.ITEM_CODE " +
                                            "INNER JOIN T_RESULT ON M_ITEM.ITEM_NUMBER = T_RESULT.ITEM_NUMBER " +
                                            "WHERE M_PLAYER.NUMBER_CARD = @number AND "+
                                            "T_RESULT.NUMBER_CARD = @number AND "+
                                            "M_ITEM.ITEM_NUMBER = M_PLAYER.ITEM_CODE";

                        command.Parameters.Add("@number", System.Data.SqlDbType.NVarChar);
                        command.Parameters["@number"].Value = playerInfo.BibNumber;
                        //command.Parameters.Add("@itemNum", System.Data.SqlDbType.Int);
                        //command.Parameters["@itemNum"].Value = searchNo;
                        command.ExecuteNonQuery();


                        using (Microsoft.Data.SqlClient.SqlDataReader reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                seqno = 4;
                                logger.Trace("Detail:OnGetAsync() seqno = " + seqno.ToString() + " SessionID=" + sessionIdStr);
                                //int sqNo = 0;
                                //int.TryParse(reader["SEQ_NUMBER"].ToString(), out sqNo);
                                //playerInfo.SequenceNo = sqNo;
                                //playerInfo.BibNumber = myParts.StrToInt(reader["NUMBER_CARD"].ToString(), -1);
                                playerInfo.ItemNumber = myParts.StrToInt(reader["ITEM_NUMBER"].ToString(), -1);
                                playerInfo.ItemName = reader["ITEM_NAME"].ToString();
                                playerInfo.PlayerName = reader["PLAYER_NAME"].ToString();
                                playerInfo.GrossTime = reader["GROSS_TIME_DSP"].ToString();
                                playerInfo.NetTime = reader["NET_TIME_DSP"].ToString();
                                playerInfo.Place = reader["RANK"].ToString();
                                playerInfo.PlaceByAll = reader["TOTAL_RANK"].ToString();
                                find_f = true;
                                playerInfo.GrossTime = myParts.RoundingMsec(playerInfo.GrossTime);  // msec四捨五入して hh:mm:ss
                                playerInfo.NetTime = myParts.RoundingMsec(playerInfo.NetTime);      // msec四捨五入して hh:mm:ss
                                break;


                            }
                        }
                    }
                    await cont.CloseAsync();
                }
            }
            catch (Exception e)
            {
                string errTitle = "エラー";
                // "登録に無いナンバーです。\n"+
                // "ナンバーをお確かめの上、もう一度検索をしてください。\n" +
                // "※当日エントリー内容を健康した場合、速報が閲覧できない場合がございます。";
                string errMsg = WordInfo.GetWord("WD_0001");
                logger.Error(errMsg + " " + e.Message + " OnGet:[" + seqno.ToString() + "]" + " SessionID=" + sessionIdStr);

                return RedirectToPage("/ErrorMsg", new { errtitle = errTitle, errmsg = errMsg });
            }

            // 結果見つからない か 大会コードが無い
            if ( (find_f == false)||(playerInfo.ItemNumber==-1) )
            {
                string errTitle = "エラー";
                // "登録に無いナンバーです。\n"+
                // "ナンバーをお確かめの上、もう一度検索をしてください。\n" +
                // "※当日エントリー内容を健康した場合、速報が閲覧できない場合がございます。";
                string errMsg = WordInfo.GetWord("WD_0001");
                logger.Error(errMsg + " OnGet:[" + seqno.ToString() + "]" + " SessionID=" + sessionIdStr);

                return RedirectToPage("/ErrorMsg", new { errtitle = errTitle, errmsg = errMsg });
            }


            //----------------------------------------------------------------
            //  完走証テーブル 取得
            //----------------------------------------------------------------
            try
            {
                Microsoft.Data.SqlClient.SqlConnection cont = null;
                var sqlCont = SqlConnectionString.Instance.GetSqlConnection(dbName);
                using (cont = new Microsoft.Data.SqlClient.SqlConnection(sqlCont))
                {
                    // SQL Server から 
                    seqno = 5;
                    logger.Trace("Detail:OnGetAsync() seqno = " + seqno.ToString() + " SessionID=" + sessionIdStr);
                    await cont.OpenAsync();
                    using (Microsoft.Data.SqlClient.SqlCommand command = new Microsoft.Data.SqlClient.SqlCommand())
                    {
                        seqno = 6;
                        logger.Trace("Detail:OnGetAsync() seqno = " + seqno.ToString() + " SessionID=" + sessionIdStr);
                        command.Connection = cont;
                        command.CommandText = "SELECT * FROM M_CERTIFICATE WHERE ID = @param1";
                        command.Parameters.Add("@param1", System.Data.SqlDbType.NVarChar);
                        command.Parameters["@param1"].Value = 0;    //　本番 =0、localDeb=2
                        command.ExecuteNonQuery();

                        using (Microsoft.Data.SqlClient.SqlDataReader reader = await command.ExecuteReaderAsync())
                        {
                            while (await reader.ReadAsync())
                            {
                                seqno = 7;
                                playerInfo.PDF = (byte[])reader["PDF"];
                                playerInfo.REFERENCE_RECORD = reader["REFERENCE_RECORD"].ToString();
                                playerInfo.PRINT_POSITION1 = reader["PRINT_POSITION1"].ToString();
                                break;
                            }
                        }
                    }
                    await cont.CloseAsync();
                }
            }
            catch (Exception e)
            {
                string errTitle = "完走証取得エラー";
                // 完走証取得でエラーが起きました。
                string errMsg = WordInfo.GetWord("WD_0003");
                logger.Error(errMsg + " " + e.Message + " OnGet:[" + seqno.ToString() + "]" + " SessionID=" + sessionIdStr);

                return RedirectToPage("/ErrorMsg", new { errtitle = errTitle, errmsg = errMsg });
            }

            // PRINT_POSITION1 XMLを解析
            seqno = 8;
            logger.Trace("Detail:OnGetAsync() seqno = " + seqno.ToString() + " SessionID=" + sessionIdStr);
            if (myParts.AnalyzeXmlData(ref playerInfo) == false)
            {
                string errTitle = "エラー";
                // 完走証取得でエラーが起きました。
                string errMsg = WordInfo.GetWord("WD_0003");
                logger.Error(errMsg + " OnGet:[" + seqno.ToString() + "]" + " SessionID=" + sessionIdStr);

                return RedirectToPage("/ErrorMsg", new { errtitle = errTitle, errmsg = errMsg });
            }
            // Gross/NetTime 取得失敗していたら項目非表示
            if (playerInfo.GrossTime.Equals("00:00:00"))
            {
                playerInfo.dispResultTitle.dispGrossTime = false;
                logger.Error("GrossTime get failure. " + " OnGet:[" + seqno.ToString() + "]" + " SessionID=" + sessionIdStr);
            }
            if (playerInfo.NetTime.Equals("00:00:00"))
            {
                playerInfo.dispResultTitle.dispGrossTime = false;
                logger.Error("NetTime get failure. " + " OnGet:[" + seqno.ToString() + "]" + " SessionID=" + sessionIdStr);
            }


            // Session へ playerInfo 保存
            HttpContext.Session.Set("PlayerInfo", playerInfo);

            // GUI へ 結果反映
            seqno = 9;
            logger.Trace("Detail:OnGetAsync() seqno = " + seqno.ToString() + " SessionID=" + sessionIdStr);
            mDspRslt = playerInfo;  // mDspRslt に入れて、 cshtml 側で参照して表示
            mDspRslt.Place = mDspRslt.Place + "位";
            mDspRslt.PlaceByAll = mDspRslt.PlaceByAll + "位";

            return Page();
        }



        ///---------------------------------------------------------------
        /// <summary>
        /// OnPostAsync 処理。  Detail <input type="submit">でボタンおされたときなど
        /// </summary>
        ///---------------------------------------------------------------
        public async Task<IActionResult> OnPostAsync()
        {
            // 完走証　情報読み出し
            int seqno = 1;
            string sessionIdStr = HttpContext.Session.Id;
            logger.Info("Detail:OnPostAsync() seqno = " + seqno.ToString() + " SessionID=" + sessionIdStr);

            PlayerInfo playerInfo = new PlayerInfo();
            try
            {
                // Session から PlayerInfo 読み出し
                var session_playerInfo = HttpContext.Session.Get<PlayerInfo>("PlayerInfo");
                playerInfo = session_playerInfo;
                if(session_playerInfo == null)
                {
                    string errTitle = "エラー";
                    // 完走証作成できませんでした。
                    string errMsg = WordInfo.GetWord("WD_0004");
                    logger.Error(errMsg + " " + "PlayerInfo is null." + " OnPost:[" + seqno.ToString() + "]" + " SessionID=" + sessionIdStr);
                    return RedirectToPage("/ErrorMsg", new { errtitle = errTitle, errmsg = errMsg });
                }
            }
            catch (Exception e)
            {
                string errTitle = "エラー";
                // 完走証作成できませんでした。
                string errMsg = WordInfo.GetWord("WD_0004") ;
                logger.Error(errMsg + " " + e.Message + " OnPost:[" + seqno.ToString() + "]" + " SessionID=" + sessionIdStr);
                return RedirectToPage("/ErrorMsg", new { errtitle = errTitle, errmsg = errMsg });
            }


            // 選択された 画像ファイル 取得
            seqno = 2;
            logger.Trace("Detail:OnPostAsync() seqno = " + seqno.ToString() + " SessionID=" + sessionIdStr);
            MyParts myParts = new MyParts();
            MemoryStream myImgStream = new MemoryStream();
            if (PostedFile != null)
            {
                // 拡張子Check
                if (myParts.CheckImageType(PostedFile.FileName) == false)
                {
                    string errTitle = "エラー";
                    // 選択された画像ファイルの拡張子は、対応しておりません。
                    string errMsg = WordInfo.GetWord("WD_0008");
                    logger.Error(errMsg + " OnPost:[" + seqno.ToString() + "]" + " SessionID=" + sessionIdStr);
                    return RedirectToPage("/ErrorMsg", new { errtitle = errTitle, errmsg = errMsg });
                }

                // image を MemoryStream
                await PostedFile.CopyToAsync(myImgStream);    // Bindで取得した PostFile を stream　で Copy
                myImgStream.Position = 0; // Fix bug PdfSharpCore - link bellow "https://github.com/ststeiger/PdfSharpCore/issues/39"
            }


            //----------------------------------------------------------------
            // PDFに画像と文字列挿入 Pdf Pageへ
            //----------------------------------------------------------------
            seqno = 3;
            logger.Trace("Detail:OnPostAsync() seqno = " + seqno.ToString() + " SessionID=" + sessionIdStr);
           
            int errKind = MyParts.NO_ERR;
            byte[] pdfFileBinaryData = myParts.InsertImageAndStringIntoPDF(playerInfo, myImgStream, ref errKind);    // PDFに画像と文字列挿入

            if ((pdfFileBinaryData != null) && (errKind == MyParts.NO_ERR))
            {
                //return File(pdfFileBinaryData, "application/pdf");
                //return File(pdfFileBinaryData, "application/pdf; charset=utf-8");
                //return File(pdfFileBinaryData, "application/pdf", "output.pdf");  // 表示しないで Download のとき。

                // ヘッダ編集
                //
                //     英語ファイル名のとき。iphone chrome は "sokuhou.te...jp%2Detail" になってしまう。
                System.Net.Mime.ContentDisposition cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = "certificate.pdf",
                    Inline = true  // false = prompt the user for downloading;  true = browser to try to show the file inline
                };
                Response.Headers.Add("Content-Disposition", cd.ToString());

                //     日本語ファイル名のとき。 (iphone safari、PCはＯＫ。android未確認。 chrome は "sokuhou.te...jp%2Detail" になってしまう。
                //Response.Headers.Add("Content-Disposition", "inline; filename*=utf-8''" + System.Web.HttpUtility.UrlEncode(playerInfo.EventName + ".pdf"));

                seqno = 4;
                logger.Trace("Detail:OnPostAsync() Req show PDF page. seqno = " + seqno.ToString() + " SessionID=" + sessionIdStr);

                Response.Headers.Add("X-Content-Type-Options", "nosniff");  // X-Content-Type-Options: nosniff をつけることで、リソースのContent-Typeを無視することがなくなる
                return File(pdfFileBinaryData, "application/pdf");

            }
            else
            {
                string errTitle = "エラー";
                string errMsg = string.Empty;
                if (errKind == MyParts.ERR_IMAGE_FORMAT)
                    errMsg = WordInfo.GetWord("WD_0009");   // 選択された画像ファイルは、挿入できない画像フォーマットです。
                else
                    errMsg = WordInfo.GetWord("WD_0004");    //完走証作成できませんでした。

                logger.Error(errMsg + " OnPost:[" + seqno.ToString() + "]");
                return RedirectToPage("/ErrorMsg", new { errtitle = errTitle, errmsg = errMsg });
            }

        } // end of OnPostAsync



    } // end of DetailModel class


}
