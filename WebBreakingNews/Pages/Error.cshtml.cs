﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

// Add
using WebBreakingNews.Parts;

namespace WebBreakingNews.Pages
{
    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    [IgnoreAntiforgeryToken]    // Project作成時、はじめからこの設定だった

    ///---------------------------------------------------------------
    /// <summary>
    /// デフォルトで用意された エラー ページクラス
    /// </summary>
    ///---------------------------------------------------------------
    public class ErrorModel : PageModel
    {
        //public string RequestId { get; set; }
        //public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);

        // NLog用
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        [BindProperty]
        public string ErrTitle { get; set; }    // html title bg_color3 エラータイトル

        //[BindProperty]
        //public string ErrMsg { get; set; }      // html gray_box error エラー詳細

        [BindProperty]
        public string[] ErrMsgs { get; set; }

        private readonly ILogger<ErrorModel> _logger;   // 未使用

        ///---------------------------------------------------------------
        /// <summary>
        /// コンストラクタ
        /// </summary>
        ///---------------------------------------------------------------
        public ErrorModel(ILogger<ErrorModel> logger)
        {
            _logger = logger;
        }

        //---------------------------------------------------------------
        /// <summary>
        /// OnGet 処理。 
        /// </summary>
        ///---------------------------------------------------------------
        public void OnGet(int? errno)
        {
            SetErrMessage(errno);
        }

        ///---------------------------------------------------------------
        /// <summary>
        /// Page遷移とかでエラーがあると OnGetより先に OnPostがくる
        /// /// <param name="errno">"int?" null 許容型。Http Error No.</param>
        /// </summary>
        ///---------------------------------------------------------------
        public void OnPost(int? errno)
        {
            SetErrMessage(errno);
        }

        //---------------------------------------------------------------
        /// <summary>
        /// errno で HTTPエラーNo. と該当する場合はエラー文を表示
        /// 該当しない場合は、エラーNo.表示
        /// /// <param name="errno">"int?" null 許容型。Http Error No.</param>
        /// </summary>
        ///---------------------------------------------------------------
        void SetErrMessage(int? errno)
        {
            //RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier;

            ErrTitle = WordInfo.GetWord("WD_0011");    // "リクエストの処理中にエラーが発生しました。"
            string[] errsStr = new string[] { };

            if (((400 <= errno) && (errno <= 431)) || ((500 <= errno) && (errno <= 511)))
            {
                string enoStr = "WD_0" + errno.ToString();
                errsStr = WordInfo.GetWord(enoStr).Split('\n');                         // 1行目
                //errsStr = WordInfo.GetWord("WD_0451").Split('\n');    // for deb
                Array.Resize(ref errsStr, errsStr.Length + 1);
                if (errsStr.Length != 0)
                    errsStr[errsStr.Length - 1] = "(HTTP: " + errno.ToString() + " )";    // 2行目
            }
            else
            {
                string eStr = "HTTP:" + errno.ToString();
                Array.Resize(ref errsStr, errsStr.Length + 1);
                errsStr[0] = eStr;
            }
            ErrMsgs = errsStr;
            //logger.Error("Error page OnGet(), " + ErrMsgs);
            logger.Error("Error page OnGet()");
            foreach (var str in ErrMsgs)
                logger.Error(", " + str);
        }

    }
}
